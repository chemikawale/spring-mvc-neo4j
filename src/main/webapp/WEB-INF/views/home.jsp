<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Books Details</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <h1> BOOKS DETAILS</h1>
            </div>
             <div class="row">
                    <div id="results">
                        <div>
                            <table id="myData" class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>S/N</th>
                                        <th>AUTHOR</th>
                                        <th>CATEGORY</th>   
                                        <th>ID</th>   
                                      
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
        </div>
    </body>
</html>

<script type="text/javascript">
     ctx = "${pageContext.request.contextPath}";
     $(document).ready(function() {
   
        $.ajax({
            type: "GET",
            url: ctx + "/bookdetails",
            success: function (result) {
 
                var response = result["bookData"];
                var bookDetails = [];
                 
                $.each(response, function (i, book) {
                    bookDetails.push("<tr><td>" + (i + 1) + "</td>");
                    bookDetails.push("<td>" + book.author + "</td>");
                    bookDetails.push("<td>" + book.category + "</td>");
                    bookDetails.push("<td>" + book.id + "</td></tr>");
                
                });
                $('#myData tbody').html(bookDetails.join(""));
               },
            error: function (result) {
                alert('error');
            }
        });
    });

</script>

