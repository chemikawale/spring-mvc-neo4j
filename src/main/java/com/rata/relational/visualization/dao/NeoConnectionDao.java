package com.rata.relational.visualization.dao;

import com.rata.relational.visualization.model.BookDetails;
import com.rata.relational.visualization.model.UpdateCategory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.neo4j.driver.v1.*;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;

@Service
public class NeoConnectionDao {

    @Value(value = "${neo4j.username}")
    private String neo4jUsername;

    @Value(value = "${neo4j.password}")
    private String neo4jPassword;

    public Session session;
    public Driver driver;

    @PostConstruct
    public void init() {

        driver = GraphDatabase.driver("bolt://localhost", AuthTokens.basic(neo4jUsername, neo4jPassword));
        session = driver.session();

    }

    public List<BookDetails> GetBookProperties() {

        StatementResult result = session.run("MATCH (n:Books) RETURN n.CATEGORY AS category,n.CATEGORYID AS categoryId,n.ImageURL AS imageUrl, n.AUTHOR AS author,ID(n) as id LIMIT 20");

        List<BookDetails> response = new ArrayList<>();

        while (result.hasNext()) {
            Record record = result.next();
            BookDetails newBook = new BookDetails();

            newBook.setAuthor(record.get("author").asString());
            newBook.setCategory(record.get("category").asString());
            newBook.setCategoryId(record.get("categoryId").asString());
            newBook.setImageUrl(record.get("imageUrl").asString());
            newBook.setId(record.get("id").asInt());

            response.add(newBook);
        }

        return response;
    }

    /**
     * This method will update a book category to a generic name e.g "PERSONAL
     * ARCHIVE"
     *
     * @param request
     */
    public void UpdateBookAuthor(UpdateCategory request) {
        Map parameters = new HashMap();
        parameters.put("id", request.getNodeId());
        parameters.put("category", request.getCategoryName());

        session.run("MATCH (n:Books) WHERE ID(n) = $id SET n.CATEGORY = $category RETURN n", parameters);

    }
}
