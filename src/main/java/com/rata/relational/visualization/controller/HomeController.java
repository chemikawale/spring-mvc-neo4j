package com.rata.relational.visualization.controller;

import com.rata.relational.visualization.dao.NeoConnectionDao;
import com.rata.relational.visualization.model.BookDetails;
import com.rata.relational.visualization.model.UpdateCategory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class HomeController {

    @Autowired
    private NeoConnectionDao neoConnection;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public ModelAndView home() {

        ModelAndView mav = new ModelAndView("/views/home");

        return mav;

    }

    @RequestMapping(value = "/bookdetails", method = RequestMethod.GET, produces = "application/json")
    public Map<String, List> getBookData() {
        Map bookResponse = new HashMap();
        List<BookDetails> bookProperties = neoConnection.GetBookProperties();
        bookResponse.put("bookData", bookProperties);
        return bookResponse;

    }

    @RequestMapping(value = "/updatecategory", method = RequestMethod.POST, consumes = "application/json;charset=UTF-8")
    public Map UpdateCategory(@RequestBody UpdateCategory request) {
        Map bookResponse = new HashMap();
        neoConnection.UpdateBookAuthor(request);
        bookResponse.put("bookData", "successful");
        return bookResponse;

    }

}
