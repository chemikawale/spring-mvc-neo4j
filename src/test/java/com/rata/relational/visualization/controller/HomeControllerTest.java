package com.rata.relational.visualization.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rata.relational.visualization.model.UpdateCategory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
public class HomeControllerTest {

    @Autowired
    private WebApplicationContext wac;

    private ObjectMapper mapper = new ObjectMapper();
    private MockMvc mockMvc;

    public HomeControllerTest() {
    }

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();

    }

    /**
     * Test of get BookBasicDataDetails method, of class ClientController.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBookBasicDataDetails() throws Exception {

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.get("/bookdetails");

        this.mockMvc.perform(builder)
                  .andDo(print())
                  .andExpect(status().isOk())
                  .andExpect(content().contentType("application/json"))
                  .andExpect(jsonPath("$bookData[0].author").exists())
                  .andExpect(jsonPath("$bookData[0].category").exists())
                  .andExpect(jsonPath("$bookData[0].categoryId").exists())
                  .andExpect(jsonPath("$bookData[0].imageUrl").exists());

    }

    /**
     *
     * @throws Exception
     */
    @Test
    public void testBookCategoryUpdate() throws Exception {
        UpdateCategory category = new UpdateCategory();

        category.setNodeId(12);
        category.setCategoryName("Test Data");

        this.mockMvc.perform(post("/updatecategory").accept(MediaType.APPLICATION_JSON).requestAttr("nodeId", 103841).requestAttr("category", "TEST CATEGORY"));

        this.mockMvc.perform(post("/updatecategory")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(mapper.writeValueAsString(category))
                  .accept(MediaType.APPLICATION_JSON))
                  .andExpect(jsonPath("$bookData").exists())
                  .andExpect(jsonPath("$bookData").value("successful"));

    }

}
